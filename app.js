var mysql = require("mysql");
var path = require("path");
var express = require("express");
var q = require("q");

const MAX_PAGE_PER_RECORD = 50;

// create a connection pool passing in connection info.
var pool = mysql.createPool({
    host: "localhost",
    port: "3306",
    user: "root",
    password: "password@123",
    database: "employees",
    connectionLimit: 4
});

const  PORT = "port";

var app = express();

const PAGINATION_FOR_BOOKS = "select id, author_lastname, author_firstname, title, cover_thumbnail, " +
    "modified_date, created_date, is_deleted from books LIMIT ?,?";

const TOTAL_BOOKS_CNT = "select COUNT(*) as totalCount from books";


var makeQuery = function (sql, args, pool) {
    var defer = q.defer();

    pool.getConnection(function (err, connection) {
        if (err) {
            return defer.reject(err);
        }
        connection.query(sql, args || [], function (err, result) {
            connection.release();
            if (err) {
                return defer.reject(err);
            }
            defer.resolve(result);
        })
    });
    return defer.promise;
};

app.get("/api/books/totalcnt/",  function(req,res) {
    makeQuery(TOTAL_BOOKS_CNT , [], pool)
        .then(function (totalCnt) {
            res.status(200).json(totalCnt[0]);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();
        });
});

app.get("/api/books/pagination/:offset/:pgrecord",  function(req,res){
    var offset = req.params.offset;
    var noOfRecPerPage = req.params.pgrecord;

    if(noOfRecPerPage >= 50){
        noOfRecPerPage = MAX_PAGE_PER_RECORD
    }
    makeQuery(PAGINATION_FOR_BOOKS , [parseInt(offset), parseInt(noOfRecPerPage)], pool)
    .then(function (books) {
        res.status(200).json(books);
    })
    .catch(function (err) {
        console.log(err);
        res.status(500).end();
    });

});

app.use(express.static(path.join(__dirname, "public")));
app.use("/bower_components", express.static(path.join(__dirname, "bower_components")));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get(PORT) , function(){
    console.info("App Server started on " + app.get(PORT));
});