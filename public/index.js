/**
 * Created by phangty on 29/7/16.
 */
(function(){
    angular
        .module("PaginationApp", [])
        .controller("paginationCtrl", paginationCtrl);

    paginationCtrl.$inject  = ["$http", "$q", "dbService"];

    function paginationCtrl($http, $q, dbService){
        var vm = this;
        vm.results = [];
        vm.pageIndex = 0;
        const recordPerPage = 12;
        vm.totalRecords = 0;
        vm.totalPages = 0;
        vm.currentPage = 1;

        var defer = $q.defer();
        vm.err = null;

        dbService.getTotalBooks()


        dbService.paging(vm.pageIndex, recordPerPage)
            .then(function (results) {
                vm.results = results.data;
            })
            .catch(function (err) {
                console.error(err);
            });
        
        dbService.getTotalBooks()
            .then(function (results) {
                vm.totalRecords = results.data.totalCount;
                vm.totalPages = Math.ceil(vm.totalRecords/recordPerPage);
                console.info(vm.totalPages);
            })
            .catch(function (err) {
                console.error(err);
            });


        vm.next = function () {
            vm.currentPage = vm.currentPage + 1;
            vm.pageIndex = vm.pageIndex + recordPerPage;
            dbService.paging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });
        };

        vm.previous = function () {
            vm.currentPage = vm.currentPage - 1;
            vm.pageIndex = vm.pageIndex - recordPerPage;
            dbService.paging(vm.pageIndex, recordPerPage)
                .then(function (results) {
                    vm.results = results.data;
                })
                .catch(function (err) {
                    console.error(err);
                });
        };

    }

})();