/**
 * Created by phangty on 29/7/16.
 */
(function () {
    angular.module("PaginationApp")
        .service("dbService", dbService);

    dbService.$inject = ["$http", "$q"];

    function dbService($http, $q) {
        var service = this;

        service.getTotalBooks = function (callback){
            var defer = $q.defer();
            $http.get("/api/books/totalcnt/")
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                    defer.reject(err);
            });
            return defer.promise;
        };

        service.paging = function (offset, recPerpage, callback){
            var defer = $q.defer();
            $http.get("/api/books/pagination/" + offset + "/" + recPerpage)
                .then(function(results){
                    defer.resolve(results);
                }).catch(function(error){
                    defer.reject(err);
            });
            return defer.promise;
        };
    }
})();
